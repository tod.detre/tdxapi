import inspect
import pkgutil
from enum import IntEnum

import tdxapi.enums as enums_module
import tdxapi.managers as managers_module
import tdxapi.models as models_module
from tdxapi.managers.bases import TdxManager
from tdxapi.models.bases import TdxModel


def get_enum_classes():
    return [e for e in find_subclasses(enums_module, IntEnum) if e.__tdx_type__]


def get_manager_classes():
    return find_subclasses(managers_module, TdxManager)


def get_model_classes():
    return find_subclasses(models_module, TdxModel)


def get_endpoints():
    endpoints = []

    for manager in get_manager_classes():
        for _, member in inspect.getmembers(manager):
            try:
                endpoints.append((member.method, member.url))
            except AttributeError:
                continue

    return endpoints


def find_subclasses(search_module, search_class):
    subclasses = set()

    for importer, modname, is_pkg in pkgutil.walk_packages(
        path=search_module.__path__, prefix=search_module.__name__ + "."
    ):
        module = __import__(modname, fromlist="dummy")

        for name, class_ in inspect.getmembers(module, inspect.isclass):
            if issubclass(class_, search_class) and class_ != search_class:
                subclasses.add(class_)

    return list(subclasses)


def to_snake_case(text):
    snake = []

    for index, char in enumerate(text):
        if char.isupper():
            # Unless we are at the beginning of the name, or the previous
            # character was also an uppercase, we will start a new word
            # by inserting an underscore...
            if index != 0 and not text[index - 1].isupper():
                snake.append("_")

            # ...and then append lowered char
            snake.append(char.lower())

        else:
            snake.append(char)

    return "".join(snake)
