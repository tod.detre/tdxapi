import attr
import requests
from bs4 import BeautifulSoup

import tools

base_url = "https://app.teamdynamix.com/TDWebApi/Home"

known_enums = tools.get_enum_classes()
known_enum_types = [e.__tdx_type__ for e in known_enums]
known_models = tools.get_model_classes()
known_model_types = [m.__tdx_type__ for m in known_models]
known_type_mapping = {
    "Guid": "str",
    "Int32": "int",
    "String": "str",
    "DateTime": "datetime",
    "Boolean": "bool",
    "Double": "float",
    "Decimal": "float",
    "Dictionary<String, Object>": "Dict[str, Any]",
}


@attr.s
class TdxApiObject(object):
    tdx_type = attr.ib()
    name = attr.ib(default=None)
    is_enum = attr.ib(default=False)
    summary = attr.ib(default=None)
    properties = attr.ib(factory=list)
    property_overrides = attr.ib(factory=dict)

    def __attrs_post_init__(self):
        known_model = None

        for model in known_models:
            if model.__tdx_type__ == self.tdx_type:
                known_model = model

        if not known_model:
            return

        for field in [f for f in attr.fields(known_model) if f.repr]:
            self.property_overrides[field.metadata["tdx_name"]] = {
                "name": field.name,
                "default": field.default,
            }

    def add_property(self, api_property):
        existing_property = self.property_overrides.get(api_property.name, None)

        if existing_property:
            api_property.overrides = existing_property

        self.properties.append(api_property)

    def has_datetime_type(self):
        for prop in self.properties:
            if prop.type_value == "DateTime":
                return True

        return False

    def has_guid_type(self):
        for prop in self.properties:
            if prop.type_value == "Guid":
                return True

        return False

    def has_custom_attributes(self):
        for prop in self.properties:
            if prop.is_attr_list:
                return True

        return False

    def known_models_found(self):
        models = []

        for prop in self.properties:
            if prop.is_known_model:
                class_name = prop.type_value.split(".")[-1]
                models.append((class_name, tools.to_snake_case(class_name)))

        return models

    def known_enums_found(self):
        models = []

        for prop in self.properties:
            if prop.is_known_enum:
                class_name = prop.type_value.split(".")[-1]
                models.append((class_name, tools.to_snake_case(class_name)))

        return models


@attr.s
class TdxApiProperty(object):
    name = attr.ib(default=None)
    editable = attr.ib(default=False)
    required = attr.ib(default=False)
    summary_lines = attr.ib(default=attr.Factory(list))
    type_value = attr.ib(default=None)
    is_list = attr.ib(default=False)
    is_known_model = attr.ib(default=False)
    is_known_enum = attr.ib(default=False)
    is_attr_list = attr.ib(default=False)
    overrides = attr.ib(default=attr.Factory(dict))

    def set_type_value(self, value):
        # <type>[]
        if "[]" in value:
            self.is_list = True
            self.type_value = value[:-2]
        else:
            self.type_value = value

        # If the type value is a number this is an enum object so skip other checks
        if value.isdigit():
            return

        if self.type_value == "TeamDynamix.Api.CustomAttributes.CustomAttribute":
            self.is_attr_list = True
        elif self.type_value in known_model_types:
            self.is_known_model = True
        elif self.type_value in known_enum_types:
            self.is_known_enum = True
            print(f"{self.snake_name} is an enum type: {value}")
        elif self.type_value not in known_type_mapping.keys():
            print(f"{self.snake_name} is an unknown type: {value}")

    @property
    def snake_name(self):
        return self.overrides.get("name", tools.to_snake_case(self.name))

    @property
    def default(self):
        return self.overrides.get("default", "None")

    @property
    def annotation(self):
        if self.is_attr_list:
            return "List[Tuple[int, Any]]"

        annotation_template = "{0}"

        if self.is_list:
            annotation_template = "List[{0}]"

        return annotation_template.format(
            "int" if self.is_known_enum else known_type_mapping[self.type_value]
        )


def get_api_object(tdx_type):
    url = base_url + f"/type/{tdx_type}"

    obj = TdxApiObject(tdx_type)

    # Use the last part of the TDX object name as the class name
    obj.name = url.split(".")[-1]

    # Parse the html from the object's documentation page
    html = BeautifulSoup(requests.get(url).text, "html.parser")

    # TDX object summary, found in the first <p> tag
    obj.summary = " ".join(html.find("p", class_="lead").text.split())

    # Get table containing property information
    table = html.find(id="properties-table")

    # if property table was not found, this is probably an enum
    if table is None:
        obj.is_enum = True
        table = html.find(id="fields-table")

    # Loop through all property rows in table
    for tr in table.find_all("tr", recursive=False):
        p = TdxApiProperty()

        # Get name from anchor tag
        td_name = tr.find("td", class_="js-name")
        p.name = td_name.a.text.strip()

        # Get editable by looking for glyphicon
        try:
            td_name = tr.find("td", class_="js-editable")
            if td_name.find("span", class_="glyphicon-pencil"):
                p.editable = True
        except AttributeError:
            # Some models do not have this column
            p.editable = False

        # Get required by looking for glyphicon
        try:
            td_required = tr.find("td", class_="js-required")
            if td_required.find("span", class_="glyphicon-ok-circle"):
                p.required = True
        except AttributeError:
            # Some models do not have this column
            p.required = False

        # Get type from td tag
        td_type = tr.find("td", class_="js-typevalue")
        p.set_type_value(td_type.text.strip())

        # Get summary from td tag
        td_summary = tr.find("td", class_="js-summary")

        # Break up summary into proper length lines
        line = ""

        for word in td_summary.text.split():
            if len(line) + (len(word) + 1) > 80:
                p.summary_lines.append(line)
                line = word
                continue
            elif len(line) == 0:
                line = word
            else:
                line = line + " " + word

        p.summary_lines.append(line)

        obj.add_property(p)

    return obj


def get_api_sections():
    html = BeautifulSoup(requests.get(base_url).text, "html.parser")

    sections = []

    # Section links grouped inside dl tag
    for section in html.find_all("dl"):
        # find all the links in this section
        for link in section.find_all("a"):
            href = link.get("href")

            sections.append(href.strip("/").split("/")[-1])

    # Auth section won't be implemented the same as the others
    sections.remove("Auth")

    return sections


def get_api_endpoints(section):
    url = base_url + f"/section/{section}"
    html = BeautifulSoup(requests.get(url).text, "html.parser")

    functions = []

    for function in html.find_all("h3"):
        if not function.string:
            continue

        method, url = function.string.strip().split(" ")
        functions.append((method, url[url.index("/api") :]))

    return functions
