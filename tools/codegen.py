import os
import sys
from pathlib import Path

from jinja2 import Environment, PackageLoader, select_autoescape

from tools import scraper, to_snake_case

# Set up Jinja2 environment and load templates
env = Environment(
    loader=PackageLoader("codegen", "templates"),
    autoescape=select_autoescape(["html", "xml"]),
)

# Default code output directory
default_output_dir = Path(__file__).resolve().parent.joinpath("output")


def output_class(td_object, template, output_dir=default_output_dir, filename=None):
    # Get specified template
    class_template = env.get_template(template)

    # Use class name as file name by default
    if not filename:
        filename = to_snake_case(td_object.name)

    out_file = output_dir.joinpath(f"{filename}.py")

    # Render template and send output to file
    class_template.stream(obj=td_object).dump(str(out_file))

    return out_file


def generate_code(*models):
    for model in models:
        obj = scraper.get_api_object(model)

        if obj.is_enum:
            file = output_class(obj, "enum.txt")
        else:
            file = output_class(obj, "class.txt")

        os.system(f"poetry run isort {file}")
        os.system(f"poetry run black {file}")

        if obj.name.endswith("Search"):
            output_class(obj, "search.txt", filename=f"{to_snake_case(obj.name)}_func")


if __name__ == "__main__":
    generate_code(*sys.argv[1:])
