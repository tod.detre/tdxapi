import time

import attr
import pytest

import tools
from tools import scraper

model_classes = tools.get_model_classes()


@pytest.mark.parametrize("model", model_classes)
def test_model_attributes(model):
    # throttle hits to web api docs
    time.sleep(1)

    api_obj = scraper.get_api_object(model.__tdx_type__)

    api_properties = [p.name for p in api_obj.properties]
    existing_properties = [f.metadata["tdx_name"] for f in attr.fields(model) if f.repr]

    assert sorted(api_properties) == sorted(existing_properties)
