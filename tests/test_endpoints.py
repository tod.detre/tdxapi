import time

import pytest

import tools
from tools import scraper

sections = scraper.get_api_sections()
existing_endpoints = tools.get_endpoints()


@pytest.mark.parametrize("section", sections)
def test_api_endpoints(section):
    # throttle hits to web api docs
    time.sleep(1)

    for endpoint in scraper.get_api_endpoints(section):
        assert endpoint in existing_endpoints
