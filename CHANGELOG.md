# Changelog

## [0.10.0] - 2022-03-02
### Added
- Add support to move ticket to another ticketing application
- Add support for assigning workflow to ticket

### Changes
- Change Enums to IntEnums. Enums now treated as integers, removing the need to use
  ```.value``` to access the value of the Enums

## [0.9.0] - 2021-09-18
### Added
- Add new attribute ```ItemUpdate.uri```
- Add new attribute ```Ticket.service_offering_id```
- Add new attribute ```Ticket.service_offering_name```
- Add ```SERVICE_ACCOUNT``` to ```UserType``` enum

### Changed
- **Breaking Change**: The following changes have been made to keep the api consistent:
  - ```User``` model attribute ```authentication_user_name``` is now
    ```authentication_username```
  - ```GroupMember``` model attribute ```authentication_user_name``` is now
    ```authentication_username```
  - ```SecurityRole``` model attribute ```user_count``` is now ```users_count```
  - ```ResourcePool``` model attribute ```resource_count``` is now ```resources_count```
  - ```Ticket``` model attribute ```sla_begin_date``` is now ```sla_start_date```  
- **Breaking Change**: Enums are no longer automatically used when creating objects. 
  Attributes that used enums are now integers and their name has been changed to
  reflect this. Any functions that used an enum as a parameter now take an integer.
  The following attribute and functions have been changed:
  - ```CustomAttributeManager.get_by_component()``` is now
    ```CustomAttributeManager.get_by_component_id()```
  - ```TicketManager.add_sla()``` parameter ```start_basis``` is now
    ```start_basis_id```
  - ```TicketManager.change_sla()``` parameter ```start_basis``` is now
    ```start_basis_id```
  - ```TicketManager.search()``` parameter ```sla_unmet_constraints``` is now
    ```sla_unmet_constraint_type_ids```
  - ```TicketManager.change_classification()``` parameter ```classification``` is now
    ```classification_id```
  - ```TicketManager.search()``` parameter ```ticket_classifications``` is now
    ```ticket_classification_ids```
  - ```TicketManager.search()``` parameter ```status_classes``` is now
    ```status_class_ids```
  - ```SecurityRoleManager.get_permission()``` parameter ```license_type``` is now
    ```license_type_id```
  - ```SecurityRoleManager.search()``` parameter ```license_type``` is now
    ```license_type_id```
  - ```SecurityRole``` model attribute ```license_type``` is now ```license_type_id```
  - ```SecurityRoleSearch``` model attribute ```license_type``` is now
    ```license_type_id```
  - ```Attachment``` model attribute ```type``` is now ```type_id```
  - ```ItemUpdate``` model attribute ```item_type``` is now ```item_type_id```
  - ```ItemUpdate``` model attribute ```type``` is now ```type_id```
  - ```ConfigurationItem``` model attribute ```backing_item_type``` is now
    ```backing_item_type_id```
  - ```User``` model attribute ```tz``` is now ```tz_id```
  - ```GroupMember``` model attribute ```tz``` is now ```tz_id```
  - ```DisplayColumn``` model attribute ```data_type``` is now ```data_type_id```
  - ```DisplayColumn``` model attribute ```sort_data_type``` is now
    ```sort_column_data_type_id```
  - ```DisplayColumn``` model attribute ```aggregate``` is now
    ```aggregate_function_id```
  - ```DisplayColumn``` model attribute ```component``` is now
    ```component_function_id```
  - ```TicketTask``` model attribute ```type``` is now ```type_id```
  - ```TicketStatus``` model attribute ```status_class``` is now ```status_class_id```
  - ```Ticket``` model attribute ```status_class``` is now ```status_class_id```
  - ```Ticket``` model attribute ```classification``` is now ```classification_id```
  - ```Ticket``` model attribute ```parent_classification``` is now
    ```parent_classification_id```
- **Breaking Change**: The following changes have been made to attributes to reflect
  there data type or functionality better:
  - ```ResourcePoolManager.search()``` parameter ```name``` is now ```name_like```
  - ```TicketManager.search()``` parameter ```converted_to_task``` is now
    ```is_converted_to_task```
  - ```Report``` model attribute ```displayed_columns``` is now ```columns```
  - ```Report``` model attribute ```sort_order``` is now ```sort_column```
  - ```Report``` model attribute ```chart_settings``` is now ```chart_columns```  
- **Breaking Change**: The following redundant prefixes have been removed:
  - ```ChartSetting``` model attribute ```column_label``` is now ```label```
  - ```ChartSetting``` model attribute ```column_name``` is now ```name```
  - ```OrderByColumn``` model attribute ```column_label``` is now ```label```
  - ```OrderByColumn``` model attribute ```column_name``` is now ```name```
  - ```Ticket``` model attribute ```is_sla_respond_by_violated``` is now
    ```is_respond_by_violated```
  - ```Ticket``` model attribute ```is_sla_resolve_by_date``` is now
    ```is_resolve_by_date```

### Fixed
- ```ReportManager.search()``` parameter ```report_source_id``` updated to
  ```source_id``` to match model

## [0.8.0] - 2021-01-10
### Added
- Support for Location API.

### Changed
- **Breaking Change**: All ```postal_code``` attributes renamed to ```zip``` for
  consistency.
- **Breaking Change**: All ```custom_attributes``` attributes renamed to
  ```attributes``` for consistency.

### Fixed
- Retrieving some models via ```get()``` resulted in an error when attributes with no
  value were returned by the TeamDynamix API.

## [0.7.0] - 2020-12-02
### Added
- Support for Group API
- ```ItemUpdate.body_text``` to get a plain text version of the update text.
- New keyword argument ```with_content``` to ```Attachment.get()``` which will
  automatically retrieve the content of the Attachment and make it available in the
  model. Note: this uses two separate API calls.

### Changed
- **Breaking Change**: ```cascade_status``` renamed to ```on_update_cascade_status``` to
  clearly communicate this only applies to existing objects when calling
  ```ProductTypeManager.save()```.
- **Breaking Change**: Application level ```get_feed()``` methods renamed to
  ```get_feed_page()``` to indicate that the results are paged.
- **Breaking Change**: Model level ```get_feed()``` methods renamed to
  ```get_feed_entries()``` to indicate that only the entries are returned.
- **Breaking Change**: ```Asset.tag``` renamed to ```Asset.service_tag```.
- **Breaking Change**: ```Report.data_rows``` renamed to ```Report.data```.
- **Breaking Change**: Renamed all ```pic_path``` properties to
  ```profile_image_file_name``` to accurately describe the data.
- **Breaking Change**: ```TicketManager.get_forms()``` and
  ```AssetManager.get_forms()``` moved to ```TicketApplication.get_ticket_forms()``` and
  ```AssetApplication.get_asset_forms()``` respectively.
- By default, ```max_results``` for search functions is set to 0 to return all results.

## [0.6.0] - 2020-11-10
### Added
- Support for Ticket API

## [0.5.1] - 2020-10-28
### Changed
- Updated attrs dependency

## [0.5.0] - 2020-10-21
### Added
- Support for Ticket Status API
- Support for Ticket Task API
- Applications can be retrieved by index with ```TdxClient.apps[app_id]```.
  ```TdxClient.asset_app()``` and ```TdxClient.ticket_app()``` are still available and
  will likely result in better code assistance from IDEs.

### Changed
- **Breaking Change**: ```Asset.add_comment()``` and
  ```ConfigurationItem.add_comemnt()``` have been renamed to ```add_feed_entry()``` to
  have a consistent name for adding to an object's feed across multiple areas.
- **Breaking Change**: ```CustomAttributeChoiceManager.all()``` has been renamed to
  ```CustomAttributeChoiceManager.get_by_attribute()``` to more clearly describe the
  results returned.
- **Breaking Change**: The following methods have had their arguments reordered to put
  the most relevant data first. This will also keep the API more consistent in the
  future when these methods are available from within model objects:
  - ```AssetManager.add_comment()```
  - ```AssetManager.add_resource()```
  - ```AssetManager.remove_resource()```
  - ```AssetManager.add_to_ticket()```
  - ```AssetManager.remove_from_ticket()```
  - ```AssetManager.add_attachment()```
  - ```ConfigurationItemManager.add_comment()```
  - ```ConfigurationItemManager.remove_relationship()```
  - ```ConfigurationItemManager.add_relationship()```
  - ```ConfigurationItemManager.add_attachment()```
  - ```CustomAttributeChoiceManager.get()```
  - ```CustomAttributeChoiceManager.get_by_attribute()```
  - ```CustomAttributeChoiceManager.delete()```

### Fixed
- Error when a 500 status code was returned from the TeamDynamix

## [0.4.1] - 2020-10-14
### Changed
- Updated project metadata

## [0.4.0] - 2020-10-14
### Added
- Support for Ticket Impact API
- Support for Ticket Priority API
- Support for Ticket Source API
- Support for Ticket Type API
- Support for Ticket Urgency API

### Changed
- **Breaking Change**: ```get_all()``` methods have been renamed to ```all()```.
- **Breaking Change**: ```AssetApplication.asset_statuses``` has been renamed to
  ```AssetApplication.statuses```.

## [0.3.0] - 2020-10-13
### Changed
- When retrieving data ```CustomAttribute.value``` now returns the appropriate type
  based on field type of the custom attribute. Date and DateTime fields will return a
  ```DateTime``` object. Attributes that accept a single choice will return an
  ```Int``` and attributes that accept multiple choices will return a ```List[Int]```.

### Fixed
- Model and Enum tests no longer fail when the order of the attributes in the TDWebApi
  documentation changes.

### Removed
- Removed ability to open TDWebApi documentation with a call to ```docs()```.

